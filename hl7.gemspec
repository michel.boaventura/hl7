lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'hl7/version'

Gem::Specification.new do |spec|
  spec.name          = "hl7"
  spec.version       = HL7::VERSION
  spec.authors       = ["Michel Boaventura"]
  spec.email         = ["michel.boaventura@gmail.com"]

  spec.summary       = "HL7 protocol implementation"
  spec.description   = "A minimal ruby's HL7 protocol implementation"
  spec.homepage      = "http://github.com/michelboaventura/hl7"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.9"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec", "~> 3.3"
  spec.add_development_dependency "guard-rspec", "~> 4.6"
end
